package info.slaven.stella;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HomeController {

    @GetMapping("/home")
    @Timed(value = "home.time", description = "Time taken to process request")
    public ResponseEntity<String> home() {
        log.info("Received /home request...");
        return ResponseEntity.ok().body("Stella.");
    }

    @GetMapping("/process")
    public ResponseEntity<String> process() {
        var message = "Something went wrong.";
        log.error(message);
        return ResponseEntity.internalServerError().body(message);
    }
}
