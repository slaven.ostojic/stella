# Stella

<a href="https://gitlab.com/slaven.ostojic/stella" target="_blank">
    <img src="stella.png" alt="Logo" width="160"/>
</a>

> Telemetry via Prometheus and Grafana

- Java 17
- Spring Boot
- Prometheus
- Grafana
- Loki
- Fluent Bit
- Docker
- Gradle
- Jib (Google Container Tools)

## Backend

#### Build and run

```shell
./gradlew build
./gradlew bootRun
```

#### Build as a Docker image

```shell
./gradlew jibDockerBuild
```

#### Run with Docker Compose

```shell
docker-compose up backend
```

## API

#### Invocation endpoints:

http://localhost:8080/home  
http://localhost:8080/process

#### Spring Boot actuator endpoints:

http://localhost:8080/actuator/info  
http://localhost:8080/actuator/health  
http://localhost:8080/actuator/prometheus

## Telemetry

#### Prometheus:

http://localhost:9090/graph?g0.expr=home_time_seconds_sum&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h&g1.expr=home_time_seconds_count&g1.tab=1&g1.stacked=0&g1.show_exemplars=0&g1.range_input=1h

`prometheus.yml` in `data/prometheus` folder contains basic configuration.

#### Grafana

http://localhost:3000/d/hirpY9T4k/stella?from=now-5m&to=now&orgId=1&refresh=5s

All Grafana related data are stored in `grafana.db` in `data/grafana` folder.

**Username**: admin  
**Password**: admin

##### Dashboard

Initial data is used to set up dashboard with application up-time, CPU usage, count and sum of HTTP API calls and log events.
Besides that, Grafana shows application logs via Loki and Fluent Bit. Configuration is places in `data/fluent` directory.
